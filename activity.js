let http = require("http");

let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}

];

http.createServer(function(request, response){

    if(request.url === "/" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Welcome to Booking System");

    } else if (request.url === "/profile" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'text/plain'});
        response.end("Welcome to your profile!");

    } else if (request.url === "/courses" && request.method === "GET"){

        response.writeHead(200,{'Content-Type':'application/json'});
        response.end("Here's our courses available\n" + JSON.stringify(courses));

    } else if (request.url === "/addcourse" && request.method === "POST"){

        let requestBody = "";

        request.on('data',function(data){
			requestBody += data;
		})

        request.on('end',function(){
			requestBody = JSON.parse(requestBody);

			courses.push(requestBody);

			response.writeHead(200,{'Content-Type':'application/json'});
            response.end("Added a course to our resources! Here's the new set of courses:\n" + JSON.stringify(courses));
			
		})
    } else if (request.url === "/updatecourse" && request.method === "PUT"){

        response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Update a course to our resources");

    } else if (request.url === "/archivecourses" && request.method === "DELETE"){

        response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end("Archive courses to our resources");
    }
}).listen(4000);